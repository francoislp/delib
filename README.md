# DElib - A C++ density evolution library for LDPC codes
---

This library performs density evolution for an LDPC code ensemble. At
the moment it only supports an Offset Min-Sum decoder for regular
codes.

### TODO

- DElib depends on a "config" submodule, and as a result any client
  code needs to also have that submodule, which is quite awkward. This
  can be fixed by making the submodule into a library.


### Dependencies

DElib relies on a git submodule, hosted on github. If the project has been cloned without it (by executing git clone without --recursive), the submodules can be retrieved in a second step by executing the following command from the project's base directory:

```sh
> git submodule update --init
```

### Build Instructions

The library can be built using cmake. First create a `build` directory.

From the project top-level directory:
```sh
> mkdir build
> cd build
> cmake ..
```

Special situations:

- If there are several versions of boost installed, use
  `-DBOOST_ROOT=<mydir>` when invoking cmake to indicate the boost root directory to be
  used (where `<mydir>` is the directory containing the include/ and lib/ subdirectories).

Finally:
```sh
> make && make install
```

### Using the library

The project installs a cmake configuration script that should allow
the library to be located automatically by a cmake build script using
`find_package(DE)` or `find_package(DE REQUIRED)` depending on whether
the library is optional or required. This script then defines the
variables `DE_FOUND`, `DE_INCLUDE_DIR`, `DE_LIBRARIES`.
