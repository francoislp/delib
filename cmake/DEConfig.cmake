# Copyright 2021 Francois Leduc-Primeau

# Try to find the DE library. Sets the following variables:
# - DE_FOUND: DE library was found
# - DE_INCLUDE_DIR: DE lib include directory
# - DE_LIBRARIES: DE lib path

# The directory containing the main header file is the include dir
find_path(DE_INCLUDE_DIR
  DE/DE.hpp
  )

if(${DE_FIND_REQUIRED} AND ("${DE_INCLUDE_DIR}" MATCHES "NOTFOUND"))
  message(FATAL_ERROR "The DE library was not found")
endif()

find_library(DE_LIBRARIES DE)

if(${DE_FIND_REQUIRED} AND ("${DE_LIBRARIES}" MATCHES "NOTFOUND"))
  message(FATAL_ERROR "The DE library was not found")
endif()
