#ifndef DE_hpp_
#define DE_hpp_

#include "config/config.hpp"
#include "DE/deviation/LLRDeviationPMF.hpp"
#include "DE/DEUnit.hpp"
#include "DE/DEVNUnit.hpp"
#include "DE/PMF_functions.hpp"
#include <vector>
#include <string>
#include <stdexcept>
#include <memory>

// debug flags
//#define DEBUG_DEV
#define DEBUG_DEV_MCSIZE 100000000
#ifdef DEBUG_DEV
#include <boost/random/mersenne_twister.hpp>
#endif

namespace DE {

class DE {
public:

	DE();

	~DE();

	/// Copy constructor
	DE(const DE& other) {
		doCopy(other);
	}

	/// Copy-assignment operator
	DE& operator=(const DE& other) {
		doCopy(other);
		return *this;
	}

	void doCopy(const DE& other) {
		_initialized=         other._initialized;
		_computeVNTotalDistr= other._computeVNTotalDistr;
		_QLLRmagn=            other._QLLRmagn;
		_Q=                   other._Q;
		_QLLRtotal=           other._QLLRtotal;
		_Qtotal=              other._Qtotal;
		_QLLRfract=           other._QLLRfract;
		_fractMult=           other._fractMult;
		_chDistr=             other._chDistr;
		_d_v=                 other._d_v;
		_d_c=                 other._d_c;
		_omsOffset=           other._omsOffset;
		_chPMF=               other._chPMF;
		_msgPMF=              other._msgPMF;
		_overrideMsgDistr=    other._overrideMsgDistr;
		_msgDistrInitialPerr= other._msgDistrInitialPerr;
		_applyDev=            other._applyDev;
		_applyDevTotal=       other._applyDevTotal;
		_pLLRDev= 0; //TODO: should be able to copy the deviation generators too?
		_pLLRDevTotal= 0;
		_iterCount=           other._iterCount;

		// copy DEUnit objects
		_pCnUnit= unitPtr_t(other._pCnUnit->getCopy());
		_pVnUnit= vnUnitPtr_t(other._pVnUnit->getCopy());
		_pDevUnitMsg.reset(nullptr);
		_pDevUnitTotal.reset(nullptr);
	}
	
	/**
	 * Loads required parameters from a config file.
	 *@param conf  ExitGen configuration.
	 *@throws key_not_found  if a required configuration item is missing
	 */
	void loadParameters(config const& conf);

	/// Whether loadParameters has been called successfully
	bool initialized() { return _initialized; }

	/**
	 * Determines whether the algorithm specified by "conf" is
	 * compatible with the base algorithm specified when calling
	 * loadParameters().
	 */
	bool compatible(config const& conf);

	/**
	 * Sets the deviation that will be applied at the end of the
	 * iteration on subsequent calls to iteration().
	 *@param pmfFile  Filepath to the conditional deviation distribution file.
	 */
	void applyDev(std::string const& pmfFile);

	/**
	 * Sets the deviation that will be applied on the VN total on
	 * subsequent calls to iteration() (if the VN total distribution is
	 * being computed).
	 */
	void applyDevTotal(std::string const& pmfFile);

	/**
	 * Initializes the PMFs.
	 */
	void DEInit();

	/// Update the message PMF to reflect one additional decoding iteration.
	void iteration();
	
	// These methods allow overriding the values of some parameters:
	/**
	 * Sets Q to the value specified, and Qtotal to preserve (roughly)
	 * the same ratio of Q/Qtotal as in the config file.
	 */
	void setQ(int Q) {
		if(!_initialized) throw std::runtime_error("parameters not loaded");
		_Q= Q;
		_Qtotal= _Q * (1 << (_QLLRtotal-1-_QLLRmagn));
	}
	
	void setChDistr(double p) {_chDistr= p;}

	/**
	 * Overrides the initial message distribution with a 1-D Normal
	 * distribution having error probability "p".
	 */
	void setInitialMsgDistr(double p) {
		if(p<0.0 || p>1.0) throw std::runtime_error("invalid probability value");
		_overrideMsgDistr= true;
		_msgDistrInitialPerr= p;
	}

	/**
	 * Turns on/off the generation of the VN total PMF. Must be called
	 * before DEInit().
	 */
	void computeTotalPMF(bool flag) { _computeVNTotalDistr= flag; }

	// Get methods:
	int getQ() { return _Q; }
	int getQtotal() { return _Qtotal; }

	/// Returns the number of iterations that have been performed.
	int iterationCount() { return _iterCount; }

	double currentErrProb() {
		if(_iterCount<0) throw std::runtime_error("DE has not been initialized!");
		return PMFerror(_msgPMF);
	}

	double curTotalErrProb() {
		if(_iterCount<0) throw std::runtime_error("DE has not been initialized!");
		if(!_computeVNTotalDistr) throw std::runtime_error("VN total PMF has not been computed");
		return PMFerror(_totalPMF);
	}

	/**
	 * Prints each element of the message PMF, separated by two spaces
	 * (including two leading spaces).
	 */
	void printMsgPMF(std::ostream& os) {
		if(_iterCount<0) throw std::runtime_error("DE has not been initialized!");
		for(int j=0; j<_msgPMF.size(); j++) os << "  " << _msgPMF[j];
		os << std::endl;
	}

	/**
	 * Prints each element of the VN total PMF, separated by two spaces
	 * (including two leading spaces).
	 */
	void printTotalPMF(std::ostream& os) {
		if(_iterCount<0) throw std::runtime_error("DE has not been initialized!");
		if(!_computeVNTotalDistr) throw std::runtime_error("VN total PMF has not been computed");
		for(int j=0; j<_totalPMF.size(); j++) os << "  " << _totalPMF[j];
		os << std::endl;
	}
	
private:

	// ---------- Data ----------

	typedef std::unique_ptr<DEUnit> unitPtr_t;
	typedef std::unique_ptr<DEVNUnit> vnUnitPtr_t;

	enum msgsched_t {FLOODING, ROW_LAYERED};

	bool _initialized;

	/// Whether to also compute the PMF of the VN total
	bool _computeVNTotalDistr;

	/// number of bits used to represent the magnitude of messages
	int    _QLLRmagn;
	int    _Q; // largest message magnitude

	/// number of bits used to represent (signed) VN totals
	int    _QLLRtotal;
	int    _Qtotal; // largest VN total magnitude

	/// number of fractional bits in the LLR representation
	int    _QLLRfract;
	double _fractMult; // computed from _QLLRfract

	/// the communication channel error probability
	double _chDistr;

	// VN and CN degrees
	int    _d_v, _d_c;
	
	int    _omsOffset;

	/// DEUnit that performs the CN update
	unitPtr_t _pCnUnit;

	/// DEUnit that performs the VN update
	vnUnitPtr_t _pVnUnit;

	/// DEUnit that applies deviations to messages
	unitPtr_t _pDevUnitMsg;

	/// DEUnit that applies deviations to VN totals
	unitPtr_t _pDevUnitTotal;

	/// PMF at the output of the channel
	std::vector<double> _chPMF;

	/// Current message PMF (at the output of a VN)
	std::vector<double> _msgPMF;

	/// PMF of the VN total
	std::vector<double> _totalPMF;

	bool   _overrideMsgDistr;
	double _msgDistrInitialPerr;

	/// Whether to apply deviations at the end of each DE iteration
	bool _applyDev;

	/// Whether to apply deviations to the VN total distribution
	bool _applyDevTotal;

	LLRDeviationPMF* _pLLRDev;
	LLRDeviationPMF* _pLLRDevTotal;

	int _iterCount;

	/// Message-passing schedule
	msgsched_t _msgSchedule;	

#ifdef DEBUG_DEV
	boost::mt19937* m_DBG_RNG;
#endif
	
};

} // end namespace DE

#endif
