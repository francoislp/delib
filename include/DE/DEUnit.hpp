// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef DEUnit_hpp_
#define DEUnit_hpp_

#include <vector>

namespace DE {

	/**
	 * Interface for classes that perform a density evolution update.
	 */
	class DEUnit {
	public:
		virtual ~DEUnit() {}

		virtual void deStep(std::vector<double>& pmfOut,
		                    std::vector<double>& pmfIn) = 0;

		virtual DEUnit* getCopy() const = 0;

	protected:
		DEUnit() {} // prevent direct instantiation
	};
}

#endif
