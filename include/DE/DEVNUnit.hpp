// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef DEVNUnit_hpp_
#define DEVNUnit_hpp_

#include "DEUnit.hpp"

namespace DE {

	class DEVNUnit : public DEUnit {
	public:

		/**
		 * Set the channel PMF.
		 */
		virtual void setChPMF(std::vector<double> const& chPMF) = 0;

		/**
		 * Returns a vector representing the PMF of the VN total.
		 */
		virtual std::vector<double> const& getTotalPMF() = 0;

		virtual DEVNUnit* getCopy() const = 0;

	protected:
		DEVNUnit() {} // prevent direct instantiation
	};
}

#endif
