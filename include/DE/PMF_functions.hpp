#ifndef PMF_functions_hpp_
#define PMF_functions_hpp_

namespace DE {

	/**
	 * Evaluates the saturated quantized normal PMF in [-Q,Q] at x.
	 */
	double normPMFSatAt(int x, int Q, double mean, double var);

	/**
	 * Generates a saturated quantized normal PMF. The range is
	 * assumed symmetric around 0 and is determined from the size of
	 * "pmf".
	 *@param mean  Mean of the normal distribution
	 *@param var   Variance of the normal distribution
	 *@param pmf   Vector that will contain the PMF.
	 */
	void normPMFSat(double mean, double var, std::vector<double>& pmf);

	/**
	 * Generates a quantized 1-D normal PMF with saturation.
	 *@param Q  Defines the quantization range (passed for error checking).
	 *@param rho   Mean of the normal distribution
	 *@param alpha The variance of the distribution is given by rho*alpha
	 *@param pmf  The vector that will contain the PMF
	 */
	void normPMF1Dsat(int Q, double rho, double alpha,
	                  std::vector<double>& pmf);

	/**
	 * Returns the CDF of the normal distribution with mean rho and
	 * variance var evaluated at x.
	 */
	double normCDF(double x, double mean, double var);

	/**
	 * Assigns pmfin to pmfout, saturating the probability at -Qout and
	 * +Qout.
	 *
	 *@param pmfout The vector that will contain the new PMF (will be resized).
	 *@param Qout   The range [-Qout, Qout] of the new PMF.
	 *@param pmfin  The source PMF (cannot be the same as pmfout).
	 *@param Qin    The range of the source PMF is [-Qin, Qin].
	 */
	void pmfAssignSat(std::vector<double>& pmfout, int Qout,
	                  std::vector<double>& pmfin, int Qin);

	/**
	 * Computes the discrete convolution of vectors X1 and X2, with a saturation 
	 * operation.
	 *@param out The result is placed here (this can be the same reference as X1 
	 *           or X2).
	 *@param X1
	 *@param X2
	 *@param Q  The range [-Q,Q] of the input and output PMFs.
	 */
	void convolSat(std::vector<double>& out, std::vector<double>& X1,
	               std::vector<double>& X2, int Q);

	double PMFerror(std::vector<double> const& pmf);

	/**
	 * Inverts the order of the vector elements, simulating the effect
	 * of flipping the sign of the random variable.
	 */
	void flipSign(std::vector<double>& pmfout, std::vector<double> const& pmfin);

	void checkPMF(std::vector<double> const& pmf, int Q);

	void checkPMFSilent(std::vector<double> const& pmf, int Q);

	void checkCDF(std::vector<double> const& cdf);

} // end namespace DE

#endif
