//Author: Francois Leduc-Primeau

#ifndef LLRDeviationPMF_hpp_
#define LLRDeviationPMF_hpp_

#include "config/config.hpp"
#include <string>
#include <vector>
#include <set>
#include <map>
#include <boost/random/mersenne_twister.hpp>

class LLRDeviationPMF {
public:

	/**
	 * Constructor. Automatically initializes the object by querying the
	 * configuration file (therefore it is not necessary to call
	 * setDeviationFile(...)).
	 */
	LLRDeviationPMF(config& conf);

	/**
	 * Initialize the object by accessing the configuration file. This
	 * method is called automatically by the constructor.
	 */
	void init(config& conf);

	/// Destructor
	~LLRDeviationPMF();

	/**
	 * Parses a file containing the conditional PMFs of the faulty messages.
	 */
	void setDeviationFile(std::string devfile);

	/**
	 * The input message error rate is a parameter of the deviation distribution.
	 *@throws OutOfBoundException
	 */
	void setInputErrorRate(double errRate);
	
	/**
	 * Samples a deviation from the deviation distribution and adds it
	 * to the LLR message provided.
	 *@param msg  The ideal message to which the deviation is added.
	 *@param txsign  +1 or -1 to indicate the sign of the transmitted bit 
	 *               associated with the message. In other words, +1 if a 
	 *               positive LLR message corresponds to a correct decision 
	 *               regarding the transmitted bit, and -1 otherwise.
	 *@return The faulty message.
	 *@throws std::invalid_argument if msg is outside valid range.
	 *@throws std::runtime_error if txsign is invalid.
	 */
	double applyDeviation(double msg, int txsign);

	/**
	 * Same as applyDeviation(double,int), but the message is
	 * automatically saturated to the appropriate range if necessary,
	 * and no exception is thrown.
	 *@return The faulty message.
	 *@throws std::runtime_error if txsign is invalid.
	 */
	double applyDeviationSat(double msg, int txsign);


	/**
	 * Returns the conditional PMF of the faulty message, given that the
	 * ideal message is "msg". Assumes that the tx sign is 1.
	 *@param msg  Ideal msg *in the LLR domain*.
	 *@throws std::invalid_argument  if message is outside the quantization range.
	 */
	std::vector<double> const& getPmf(double msg);

private:

	/**
	 * Converts an error rate into a key to perform table lookups.
	 */
	double errRateKey(double errRate);

	/**
	 * Converts an error rate key back to an error rate (inverse of
	 * errRateKey, if we forget the rounding).
	 */
	double errFromKey(double errKey);

	// ----- Data -----
	
	/**
	 * Input error rate key used to sample the deviation distribution.
	 * Note that we actually store the value returned by errRateKey
	 * rather than the error rate itself.
	 */
	double m_pInKey;

	/// uniform random number generator used to sample from the deviation PMF
	boost::mt19937* m_pRng;

	/// Number of quantized error rate points per decade used in the tables.
	uint m_nbErrPtsPerDec;

	/// Fractional multiplier used to convert LLR values in the deviation file.
	double m_fractMult;

	/// Range of the PMFs: [-m_Q, m_Q]
	int m_Q;

	/**
	 * Contains the set of p_in keys that were specified in the
	 * deviation file. This set is used to avoid performing
	 * extrapolation from previously interpolated points.
	 */
	std::set<double> m_originalPInKeySet;

	typedef std::set<double>::iterator setdblIt_t;

	typedef std::vector<std::vector<double> > matrix_t;
	typedef std::map<double, matrix_t > matrixmap_t;

	/**
	 * The conditional PMFs, by (error rate key, ideal value).
	 */
	matrixmap_t m_pmfMap;

};

#endif
