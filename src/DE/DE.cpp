#include "DE/DE.hpp"

#include <iostream>
#include <boost/math/special_functions/erf.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/sum_kahan.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <sstream>
//#include "../deviation/Logging.hpp" // NoiseGen requires this to be initialized

// we use the Kahan summation algorithm to make sure numerical errors
// are as small as possible (probably an overkill)
using namespace boost::accumulators;
typedef accumulator_set<double, stats<tag::sum_kahan> > kahan_acc_t;

#include "OMSCN.hpp"
#include "FloodingVN.hpp"
#include "LayeredVN.hpp"
#include "DevFromPMF.hpp"

#ifdef DEBUG_DEV
#include <boost/random/discrete_distribution.hpp>
#endif

using std::cerr; // for debug logging
using std::endl;
using std::vector;

namespace DE {

DE::DE()
	: _QLLRmagn(0),
	  _QLLRtotal(0),
	  _QLLRfract(0),
	  _fractMult(0),
	  _chDistr(0),
	  _d_v(0),
	  _d_c(0),
	  _omsOffset(0),
	  _initialized(false),
	  _computeVNTotalDistr(false),
	  _overrideMsgDistr(false),
	  _applyDev(false),
	  _applyDevTotal(false),
	  _pLLRDev(0),
	  _pLLRDevTotal(0),
	  _iterCount(-1)
{
#ifdef DEBUG_DEV
	m_DBG_RNG = new boost::mt19937(42); // seed 0 is used by LLRDeviationPMF
#endif
}

DE::~DE() {
	if(_pLLRDev != 0) delete _pLLRDev;
	if(_pLLRDevTotal != 0) delete _pLLRDevTotal;
#ifdef DEBUG_DEV
	delete m_DBG_RNG;
#endif
}

void DE::loadParameters(config const& conf) {
	_QLLRmagn = conf.parseParamUInt("QLLRmagn");
	_Q= (1 << _QLLRmagn) - 1;
	
	_QLLRtotal= conf.parseParamUInt("QLLRtotal");
	// range of the VN total (and of the channel prior)
	_Qtotal= (1 << (_QLLRtotal-1)) - 1;	  
	
	_QLLRfract= conf.parseParamUInt("QLLRfract");
	_fractMult = static_cast<double>(1 << _QLLRfract);
	
	_chDistr  = conf.parseParamDouble("ch_dist");
	_d_v      = conf.parseParamUInt("DV");
	_d_c      = conf.parseParamUInt("DC");
	_omsOffset= conf.parseParamUInt("OMSOFFSET");

	if(conf.getParamString("DE::msg_sched")=="flooding") {
		_msgSchedule= FLOODING;
	} else if(conf.getParamString("DE::msg_sched")=="row_layered") {
		_msgSchedule= ROW_LAYERED;
	} else {
		throw std::runtime_error("Invalid message-passing schedule");
	}

	_initialized= true;
}

bool DE::compatible(config const& conf) {
	if(!_initialized) throw std::runtime_error("parameters not loaded");
	return _QLLRmagn == conf.parseParamUInt("QLLRmagn") &&
		_QLLRtotal == conf.parseParamUInt("QLLRtotal") &&
		_QLLRfract == conf.parseParamUInt("QLLRfract") &&
		_chDistr == conf.parseParamDouble("ch_dist") &&
		_d_v == conf.parseParamUInt("DV") &&
		_d_c == conf.parseParamUInt("DC") &&
		_omsOffset == conf.parseParamUInt("OMSOFFSET");
}

void DE::applyDev(std::string const& pmfFile) {
	if(!_initialized) throw std::runtime_error("parameters not loaded");
	
	config conf;
	// add some parameters used by LLRDeviation
	conf.addConfElem("LLRDeviation::nbErrPtsPerDec", "1000");
	std::stringstream ss;
	ss << _fractMult;
	conf.addConfElem("LLRDeviation::fract_mult", ss.str());
	conf.addConfElem("LLRDeviation::dev_pmf_file", pmfFile);

	if(_pLLRDev != 0) delete _pLLRDev;
	
	_pLLRDev= new LLRDeviationPMF(conf);
	_pDevUnitMsg.reset(new DevFromPMF(_Q, _fractMult, _pLLRDev));
	
	_applyDev= true;
}

void DE::applyDevTotal(std::string const& pmfFile) {
	if(!_initialized) throw std::runtime_error("parameters not loaded");
	
	config conf;
	// add some parameters used by LLRDeviation
	conf.addConfElem("LLRDeviation::nbErrPtsPerDec", "1000");
	std::stringstream ss;
	ss << _fractMult;
	conf.addConfElem("LLRDeviation::fract_mult", ss.str());
	conf.addConfElem("LLRDeviation::dev_pmf_file", pmfFile);

	if(_pLLRDevTotal != 0) delete _pLLRDevTotal;

	_pLLRDevTotal= new LLRDeviationPMF(conf);
	_pDevUnitTotal.reset(new DevFromPMF(_Qtotal, _fractMult, _pLLRDevTotal));

	_applyDevTotal= true;
}

void DE::DEInit() {
	if(!_initialized) throw std::runtime_error("parameters not loaded");

	// create new DE units
	_pCnUnit.reset(new OMSCN(_Q, _d_c, _omsOffset));
	if( _msgSchedule == ROW_LAYERED ) {
		_pVnUnit.reset(new LayeredVN(_Q, _Qtotal, _d_v));
	} else {
		_pVnUnit.reset(new FloodingVN(_Q, _Qtotal, _d_v, _computeVNTotalDistr));
	}
	
  // compute parameters \rho and \alpha of the 1-D normal distribution
  double rho= boost::math::erf_inv(1.0-2.0*_chDistr);
  rho= 4.0 * rho * rho * _fractMult;
  double alpha= 2*_fractMult;
#ifndef NDEBUG
  cerr << "Channel distribution: rho="<<rho<<", alpha="<<alpha<<endl;
  cerr << "OMS offset = "<<_omsOffset <<endl;
#endif

  _chPMF.resize(2*_Qtotal+1);
  normPMF1Dsat(_Qtotal, rho, alpha, _chPMF);

  _pVnUnit->setChPMF(_chPMF);

  if(_overrideMsgDistr) {
	  double rhoMsg= boost::math::erf_inv(1.0-2.0*_msgDistrInitialPerr);
	  rhoMsg= 4.0 * rhoMsg * rhoMsg * _fractMult;
	  _msgPMF.resize(2*_Q+1);
	  normPMF1Dsat(_Q, rhoMsg, alpha, _msgPMF);
  } else {
	  pmfAssignSat(_msgPMF, _Q, _chPMF, _Qtotal);
  }

  if(_computeVNTotalDistr) {
	  _totalPMF= _chPMF;
  }

  _iterCount= 0;
  
#ifndef NDEBUG
  cerr << "Channel PMF:"<<endl;
  checkPMF(_chPMF,_Qtotal);
#endif  
}

void DE::iteration() {
	if(_iterCount<0) throw std::runtime_error("DE has not been initialized!");
	if(_applyDev && _pLLRDev==0) throw std::logic_error("deviation generator doesn't exist!");
	if(_applyDevTotal && _pLLRDevTotal==0) throw std::logic_error("missing deviation generator for VN total");
	if(_msgPMF.size()==0) throw std::logic_error("Msg PMF size should not be zero if _iterCount>=0");

	// save the error rate at the beginning of the iteration
	double curPErr= PMFerror(_msgPMF);

	int layerCount= 1;
	if(_msgSchedule == ROW_LAYERED) layerCount= _d_v;

	for(int i=0; i<layerCount; i++) {
		// CN operation
		_pCnUnit->deStep(_msgPMF, _msgPMF);
#ifndef NDEBUG
		cerr << "after CN step:"<<endl;
		checkPMF(_msgPMF,_Q);
		// debug: print CN PMF
		cerr << _iterCount+1 << "  " << PMFerror(_msgPMF);
		printMsgPMF(cerr);
#endif

		// VN operation
		_pVnUnit->deStep(_msgPMF, _msgPMF);
#ifndef NDEBUG
		cerr << "after VN step:"<<endl;
		checkPMF(_msgPMF,_Q);
#endif

		// Apply deviation on next VN message
		if(_applyDev) {
			_pLLRDev->setInputErrorRate(curPErr);
			_pDevUnitMsg->deStep(_msgPMF, _msgPMF);
		}
#ifndef NDEBUG
		cerr << "after dev step:"<<endl;
		checkPMF(_msgPMF,_Q);
#endif
		curPErr= PMFerror(_msgPMF);
	}
	
	// VN total
	if(_computeVNTotalDistr) {
		_totalPMF= _pVnUnit->getTotalPMF();
		if(_applyDevTotal) {
			_pLLRDevTotal->setInputErrorRate(curPErr);
			_pDevUnitTotal->deStep(_totalPMF, _totalPMF);
		}
	}
	  
	_iterCount++;
}

} // end namespace DE
