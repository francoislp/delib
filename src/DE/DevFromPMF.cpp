#include "DevFromPMF.hpp"
#include "DE/PMF_functions.hpp"

// we use the Kahan summation algorithm to make sure numerical errors
// are as small as possible (probably an overkill)
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/sum_kahan.hpp>
#include <boost/accumulators/statistics/stats.hpp>
using namespace boost::accumulators;
typedef accumulator_set<double, stats<tag::sum_kahan> > kahan_acc_t;


using std::vector;

namespace DE {

	DevFromPMF::DevFromPMF(int Q, double fractMult, LLRDeviationPMF* dev)
		: _Q(Q),
		  _fractMult(fractMult),
		  _pDev(dev)
	{}

	void DevFromPMF::deStep(vector<double>& pmfOut, vector<double>& pmfIn) {
		if(pmfIn.size() != 2*_Q+1) throw std::logic_error("Invalid PMF size");

#ifdef DEBUG_DEV //DEBUG: compute the PMF by MC simulation
		vector<int> histout;
		histout.resize(2*_Q+1);
		for(int i=0; i<histout.size(); i++) histout[i]= 0;

		// sample ideal output distribution
		boost::random::discrete_distribution<> pDist(pmfIn.cbegin(),
		                                             pmfIn.cend());
		for(int i=0; i < DEBUG_DEV_MCSIZE; i++) {
			int idealMsg= pDist(*m_DBG_RNG) - _Q;
			double curLLR= static_cast<double>(idealMsg) / _fractMult;
			double faultyMsg= _pDev->applyDeviation(curLLR, 1);
			int faultyMsgIndex= static_cast<int>(faultyMsg*_fractMult)+_Q;
			if(faultyMsgIndex<0 || faultyMsgIndex>2*Q) {
				throw std::logic_error("bug in debug code");
			}
			histout[faultyMsgIndex]++;
		}

		// convert histogram to PMF
		pmfOut.resize(2*_Q+1);
		for(int i=0; i<pmfOut.size(); i++) {
			pmfOut[i]= static_cast<double>(histout[i]) / DEBUG_DEV_MCSIZE;
		}
	
#else // ---------- normal implementation	-------------------------
		vector<double> tmpout; //note: can only write to pmfOut at the end!
		tmpout.resize(2*_Q+1);

		for(int x=-_Q; x<=_Q; x++) {
			kahan_acc_t acc;
			for(int y=-_Q; y<=_Q; y++) {
				double curLLR= static_cast<double>(y) / _fractMult;
				vector<double> const& curCondPMF= _pDev->getPmf(curLLR);
#ifndef NDEBUG
				checkPMFSilent(curCondPMF, _Q);
#endif
				acc( curCondPMF.at(x+_Q) * pmfIn.at(y+_Q) );
			}
			tmpout.at(x+_Q)= sum_kahan(acc);
		}
	
		pmfOut= tmpout;
#endif
	}
	
} // end namespace DE
