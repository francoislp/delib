// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef DevFromPMF_hpp_
#define DevFromPMF_hpp_

#include "DE/DEUnit.hpp"
#include "DE/deviation/LLRDeviationPMF.hpp"

namespace DE {

	/**
	 * The DE step corresponding to the
	 * family-of-conditional-distributions deviation model.
	 */
	class DevFromPMF : public DEUnit {
	public:

		DevFromPMF(int Q, double fractMult, LLRDeviationPMF* dev);

		void deStep(std::vector<double>& pmfOut, std::vector<double>& pmfIn);

		DEUnit* getCopy() const {
			throw std::logic_error("Cannot copy this class");
		}

	private:
		int _Q;
		double _fractMult;
		LLRDeviationPMF* _pDev;
	};
}
#endif
