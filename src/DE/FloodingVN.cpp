// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#include "FloodingVN.hpp"
#include "DE/PMF_functions.hpp"
#include <stdexcept>

#ifndef NDEBUG
#include <iostream>
using std::cerr;
using std::endl;
#endif

using std::vector;

namespace DE {

	FloodingVN::FloodingVN(int Q, int Q_total, int d_v)
		: _Q(Q),
		  _Qtotal(Q_total),
		  _dv(d_v),
		  _computeTotalPMF(false)
	{}

	FloodingVN::FloodingVN(int Q, int Q_total, int d_v, bool computeTotalPMF)
		: _Q(Q),
		  _Qtotal(Q_total),
		  _dv(d_v),
		  _computeTotalPMF(computeTotalPMF)
	{}

	void FloodingVN::setChPMF(vector<double> const& chPMF) {
		if(chPMF.size() != 2*_Qtotal+1) {
			throw std::runtime_error("Incorrect PMF size");
		}
		
		_chPMF= chPMF;
		if(_computeTotalPMF) _totalPMF= chPMF;
	}

	vector<double> const& FloodingVN::getTotalPMF() {
		if(!_computeTotalPMF) throw std::runtime_error("VN total PMF was not computed");
		return _totalPMF;
	}

	void FloodingVN::deStep(vector<double>& muPMF, vector<double>& lambdaPMF) {
		if(_computeTotalPMF) vnTotalPMF(_totalPMF, lambdaPMF);
		vnPMF(muPMF, lambdaPMF);
	}
	
	void FloodingVN::vnPMF(vector<double>& muPMF, vector<double>& lambdaPMF) {
		if(lambdaPMF.size() != 2*_Q+1) throw std::runtime_error("Q mismatch");

		// work with a copy of lambdaPMF defined over [-Qch, Qch]
		vector<double> tmpLambdaPMF;
		tmpLambdaPMF.resize(2*_Qtotal+1);
		for(int x=-_Qtotal; x<=_Q-1; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;
		for(int x=-_Q; x<=_Q; x++) tmpLambdaPMF.at(x+_Qtotal)= lambdaPMF.at(x+_Q);
		for(int x=_Q+1; x<=_Qtotal; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;
	
		vector<double> tmpOutPMF;
		convolSat(tmpOutPMF, _chPMF, tmpLambdaPMF, _Qtotal);
#ifndef NDEBUG
		cerr<<"(after convolution 1)"<<endl;
		checkPMF(tmpOutPMF,_Qtotal);
#endif
		for(int i=1; i<_dv-1; i++) {
			convolSat(tmpOutPMF, tmpOutPMF, tmpLambdaPMF, _Qtotal);
#ifndef NDEBUG
			cerr<<"(after convolution "<<i+1<<")"<<endl;
			checkPMF(tmpOutPMF,_Qtotal);
#endif
		}
		// saturate result to [-Q, Q]
		pmfAssignSat(muPMF, _Q, tmpOutPMF, _Qtotal);
	}

	void FloodingVN::vnTotalPMF(vector<double>& totPMF,
	                            vector<double>& lambdaPMF) {
		// Note: Very similar to vnPMF

		if(lambdaPMF.size() != 2*_Q+1) throw std::logic_error("Q mismatch");
		// total and channel beliefs share same domain
		if(totPMF.size() != 2*_Qtotal+1) throw std::logic_error("totPM size mismatch");
		
		// work with a copy of lambdaPMF defined over [-Qtotal, Qtotal]
		vector<double> tmpLambdaPMF;
		tmpLambdaPMF.resize(2*_Qtotal+1);
		for(int x=-_Qtotal; x<=_Q-1; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;
		for(int x=-_Q; x<=_Q; x++) tmpLambdaPMF.at(x+_Qtotal)= lambdaPMF.at(x+_Q);
		for(int x=_Q+1; x<=_Qtotal; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;
	
		convolSat(totPMF, _chPMF, tmpLambdaPMF, _Qtotal);
		for(int i=1; i<_dv; i++) { // VN total is the sum of d_v messages + prior
			convolSat(totPMF, totPMF, tmpLambdaPMF, _Qtotal);
		}
	}
	
} // end namespace DE
