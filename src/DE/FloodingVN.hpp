// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef FloodingVN_hpp_
#define FloodingVN_hpp_

#include "DE/DEVNUnit.hpp"

namespace DE {

	class FloodingVN : public DEVNUnit {
	public:

		/**
		 * Constructs a new DE unit that does not compute the VN total
		 * distribution.
		 *
		 *@param Q       Message domain range [-Q, Q]
		 *@param Q_total Domain range of VN total [-Q_total, Q_total]
		 *@param d_v     Variable node degree
		 */
		FloodingVN(int Q, int Q_total, int d_v);

		/**
		 * Constructs a new DE unit, with the option of computing the PMF
		 * of the VN total.
		 *
		 *@param Q       Message domain range [-Q, Q]
		 *@param Q_total Domain range of VN total [-Q_total, Q_total]
		 *@param d_v     Variable node degree
		 *@param computeTotalPMF  Set to true to compute the PMF of the total.		 
		 */
		FloodingVN(int Q, int Q_total, int d_v, bool computeTotalPMF);		

		void deStep(std::vector<double>& muPMF, std::vector<double>& lambdaPMF);

		void setChPMF(std::vector<double> const& chPMF);

		/**
		 * Returns the PMF of the VN total.
		 *
		 *@throws std::runtime_error  if the total PMF is not being computed
		 */
		std::vector<double> const& getTotalPMF();

		/**
		 * Calls the (implicit) copy constructor of the derived class and
		 * returns a base class pointer.
		 */		
		DEVNUnit* getCopy() const { return new FloodingVN(*this); }

	private:

		void vnPMF(std::vector<double>& muPMF, std::vector<double>& lambdaPMF);

		void vnTotalPMF(std::vector<double>& totPMF, std::vector<double>& lambdaPMF);
		
		// ---------- Data Members ----------
		int _Q;
		int _Qtotal;
		int _dv;
		bool _computeTotalPMF;

		std::vector<double> _chPMF;
		std::vector<double> _totalPMF;
	};
	
}

#endif
