// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#include "LayeredVN.hpp"
#include "DE/PMF_functions.hpp"
#include <stdexcept>

using std::vector;

namespace DE {

	LayeredVN::LayeredVN(int Q, int Q_total, int d_v)
		: _Q(Q),
		  _Qtotal(Q_total),
		  _dv(d_v)
	{}

	void LayeredVN::deStep(vector<double>& muPMF, vector<double>& lambdaPMF) {
		if(lambdaPMF.size() != 2*_Q+1) throw std::runtime_error("Q mismatch");
		if(muPMF.size() != 2*_Q+1) throw std::runtime_error("Q mismatch");
		if(_prevPMFs.size() > _dv-1) throw std::logic_error("Ay caramba!");

		// work with a copy of lambdaPMF defined over [-Qtotal, Qtotal]
		vector<double> tmpLambdaPMF;
		tmpLambdaPMF.resize(2*_Qtotal+1);
		for(int x=-_Qtotal; x<=_Q-1; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;
		for(int x=-_Q; x<=_Q; x++) tmpLambdaPMF.at(x+_Qtotal)= lambdaPMF.at(x+_Q);
		for(int x=_Q+1; x<=_Qtotal; x++) tmpLambdaPMF.at(x+_Qtotal)= 0;

		_prevPMFs.push_back(tmpLambdaPMF); // now has *up to* d_v elements
		vector<double> intrinsic= _prevPMFs.front();
		if(_prevPMFs.size() > _dv-1) {
			_prevPMFs.pop_front();
		}

		// PMF of extrinsic message
		auto it= _prevPMFs.begin();
		convolSat(_totalPMF, _chPMF, *it, _Qtotal);
		it++;
		while(it != _prevPMFs.end()) {
			convolSat(_totalPMF, _totalPMF, *it, _Qtotal);
			it++;
		}
		pmfAssignSat(muPMF, _Q, _totalPMF, _Qtotal);

		// finish computing the VN-total PMF
		convolSat(_totalPMF, intrinsic, _totalPMF, _Qtotal);
	}

	void LayeredVN::setChPMF(vector<double> const& chPMF) {
		if(chPMF.size() != 2*_Qtotal+1) {
			throw std::runtime_error("Incorrect PMF size");
		}
		
		_chPMF= chPMF;
		_totalPMF= chPMF;
	}	
	
} // end namespace DE
