// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef LayeredVN_hpp_
#define LayeredVN_hpp_

#include "DE/DEVNUnit.hpp"
#include <list>

namespace DE {

	class LayeredVN : public DEVNUnit {
	public:

		LayeredVN(int Q, int Q_total, int d_v);

		/**
		 * Evaluates the effect of one additional sub-iteration.
		 *
		 *@param lambdaPMF The CN-to-VN PMF in that sub-iteration.
		 *@param muPMF This parameter is overwritten with the new VN-to-CN PMF.
		 */
		void deStep(std::vector<double>& muPMF, std::vector<double>& lambdaPMF);

		void setChPMF(std::vector<double> const& chPMF);

		/**
		 * Returns the PMF of the VN total.
		 */
		std::vector<double> const& getTotalPMF() { return _totalPMF; }

		/**
		 * Calls the (implicit) copy constructor of the derived class and
		 * returns a base class pointer.
		 */		
		DEVNUnit* getCopy() const { return new LayeredVN(*this); }

	private:
		// ---------- Data Members ----------
		int _Q;
		int _Qtotal;
		int _dv;

		typedef std::vector<double> pmf_t;
		pmf_t _chPMF;
		pmf_t _totalPMF;

		std::list<pmf_t> _prevPMFs;
	};
	
}

#endif
