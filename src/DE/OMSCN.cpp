#include "OMSCN.hpp"

#include <boost/math/special_functions/binomial.hpp>
// we use the Kahan summation algorithm to make sure numerical errors
// are as small as possible (probably an overkill)
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/sum_kahan.hpp>
#include <boost/accumulators/statistics/stats.hpp>
using namespace boost::accumulators;
typedef accumulator_set<double, stats<tag::sum_kahan> > kahan_acc_t;

#ifndef NDEBUG
#include "PMF_functions.hpp"
#include <iostream>
using std::cerr;
using std::endl;
#endif

using std::vector;

namespace DE {

OMSCN::OMSCN(int Q, int d_c, int offset)
	: _Q(Q),
	  _dc(d_c),
	  _off(offset)
{
	if(_Q <= 0) throw std::runtime_error("Q cannot be <=0");
	if(_dc < 2) throw std::runtime_error("d_c should be at least 2");
	if(_off < 0) throw std::runtime_error("offset must be a positive value");
}

void OMSCN::deStep(vector<double>& lambdaPMF, vector<double>& muPMF) {
	// The CDF of mu
	vector<double> muCDF;
	muCDF.resize(2*_Q+1);
	kahan_acc_t acc;
	for(int x=-_Q; x<=_Q; x++) {
		acc(muPMF.at(x+_Q));
		muCDF.at(x+_Q)= sum_kahan(acc);
	}
#ifndef NDEBUG
	cerr << "(muCDF)" << endl;
	checkCDF(muCDF);
#endif

	// First consider the CN output "m" before applying the offset
	// Pr(m>x) for 0 <= x < Q
	vector<double> prMPos;
	prMPos.resize(_Q);
	for(int x=0; x<=_Q-1; x++) {
		kahan_acc_t acc2;
		for(int k=0; k<_dc-1; k+=2) {
			acc2(
				boost::math::binomial_coefficient<double>(_dc-1,k)
				*pow(muCDF.at(-x-1+_Q),k)*pow(1-muCDF.at(x+_Q),_dc-k-1));
		}
		prMPos.at(x)= sum_kahan(acc2);
	}
	// Pr(m<=x) for -Q <= x <= 1
	vector<double> prMNeg;
	prMNeg.resize(_Q);
	for(int x=-_Q; x<=-1; x++) {	
		kahan_acc_t acc2;
		for(int k=1; k<_dc-1; k+=2) {
			acc2(
				boost::math::binomial_coefficient<double>(_dc-1,k)
				*pow(muCDF.at(x+_Q),k)*pow(1-muCDF.at(-x-1+_Q),_dc-k-1));
		}
		prMNeg.at(x+_Q)= sum_kahan(acc2);
	}

	// PMF of the minimum "m"
	vector<double> mPMF;
	mPMF.resize(2*_Q+1);
	mPMF.at(0)= prMNeg.at(0);
	for(int x=-_Q+1; x<=-1; x++) {
		mPMF.at(x+_Q)= prMNeg.at(x+_Q) - prMNeg.at(x-1+_Q);
	}
	mPMF.at(0+_Q)= 1 - pow(1-muPMF.at(0+_Q),_dc-1);
	for(int x=1; x<=_Q-1; x++) {
		mPMF.at(x+_Q)= prMPos.at(x-1) - prMPos.at(x);
	}
	mPMF.at(_Q+_Q)= prMPos.at(_Q-1);
#ifndef NDEBUG
	cerr << "(CN: min PMF)"<<endl;
	checkPMF(mPMF,_Q);
#endif

	// Now we take the offset into account. The check node output after
	// offset addition is denoted lambda, and its pdf is indexed by x.
	lambdaPMF.resize(2*_Q+1);
	// for x<0
	for(int x=-_Q; x<-_Q+_off; x++) {
		lambdaPMF.at(x+_Q)= 0;
	}
	for(int x=-_Q+_off; x<0; x++) {
		lambdaPMF.at(x+_Q)= mPMF.at(x-_off+_Q);
	}
	// for x=0
	lambdaPMF.at(0+_Q)= 0;
	for(int m=-_off; m<=_off; m++) {
		lambdaPMF.at(0+_Q)+= mPMF.at(m+_Q);
	}
	// for x>0
	for(int x=1; x<=_Q-_off; x++) {
		lambdaPMF.at(x+_Q)= mPMF.at(x+_off+_Q);
	}
	for(int x=_Q-_off+1; x<=_Q; x++) {
		lambdaPMF.at(x+_Q)= 0;
	}
}

} // end namespace DE
