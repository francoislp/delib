// Copyright 2016 Francois Leduc-Primeau (francoislp@gmail.com)

#ifndef OMSCN_hpp_
#define OMSCN_hpp_

#include "DE/DEUnit.hpp"

namespace DE {
	class OMSCN : public DEUnit {
	public:

		/**
		 * Constructor
		 *
		 *@param Q       Message domain range [-Q, Q]
		 *@param d_c     Check node degree
		 *@param offset  Offset applied to first and second minima
		 */
		OMSCN(int Q, int d_c, int offset);

	/**
	 * Computes the PMF of a CN output message.
	 *
	 *@param lambdaPMF  Output value (can be the same reference as muPMF)
	 *@param muPMF      PMF of VN-to-CN messages
	 */		
		void deStep(std::vector<double>& lambdaPMF, std::vector<double>& muPMF);

		/**
		 * Calls the (implicit) copy constructor of the derived class and
		 * returns a base class pointer.
		 */		
		DEUnit* getCopy() const { return new OMSCN(*this); }

	private:
		int _Q;
		int _dc;
		int _off;
	};
}
#endif
