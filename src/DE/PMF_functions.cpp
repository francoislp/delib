#include <boost/math/special_functions/erf.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/sum_kahan.hpp>
#include <boost/accumulators/statistics/stats.hpp>

// we use the Kahan summation algorithm to make sure numerical errors
// are as small as possible (probably an overkill)
using namespace boost::accumulators;
typedef accumulator_set<double, stats<tag::sum_kahan> > kahan_acc_t;

#include "DE/PMF_functions.hpp"
#include <iostream>

namespace DE {

	double normPMFSatAt(int x, int Q, double mean, double var) {
		if(x<-Q || x>Q) throw std::logic_error("x is outside of range");
		double xx= static_cast<double>(x);
		if(x==-Q) return normCDF(xx+0.5,mean,var);
		if(x==Q) return 1 - normCDF(xx-0.5,mean,var);
		return normCDF(xx+0.5,mean,var) - normCDF(xx-0.5,mean,var);
	}

	void normPMFSat(double mean, double var, std::vector<double>& pmf) {
		int Q= (pmf.size()-1)/2;
		if(2*Q+1 != pmf.size()) throw std::logic_error("Invalid pmf size");

		for(int x=-Q; x<=Q; x++) {
			pmf.at(x+Q)= normPMFSatAt(x,Q,mean,var);
		}
	}

	void normPMF1Dsat(int Q, double rho, double alpha, std::vector<double>& pmf) {
		if(pmf.size() != 2*Q+1) throw std::logic_error("pmf has invalid size");

		normPMFSat(rho, rho*alpha, pmf);
	}

	double normCDF(double x, double mean, double var) {
		double y= boost::math::erf((x-mean)/sqrt(2*var));
		return 0.5 + 0.5 * y;
	}

	void pmfAssignSat(std::vector<double>& pmfout, int Qout,
	                  std::vector<double>& pmfin, int Qin) {
		if(Qout>Qin) throw std::logic_error("cannot have Qout>Qin");
		if(pmfin.size() != 2*Qin+1) throw std::logic_error("incorrect Qin");

		kahan_acc_t accPos;
		for(double x=Qout; x<=Qin; x++) {
			accPos( pmfin.at(x+Qin) );
		}
		kahan_acc_t accNeg;
		for(double x=-Qin; x<=-Qout; x++) {
			accNeg( pmfin.at(x+Qin) );
		}
		pmfout.resize(2*Qout+1);
		pmfout.at(0)= sum_kahan(accNeg);
		pmfout.at(2*Qout)= sum_kahan(accPos);
		for(double x=-Qout+1; x<=Qout-1; x++) {
			pmfout.at(x+Qout)= pmfin.at(x+Qin);
		}
	}

	void convolSat(std::vector<double>& out,
	               std::vector<double>& X1, std::vector<double>& X2,
	               int Q) {
		if(X1.size() != X2.size()) throw std::logic_error("size mismatch");
		if(X1.size() != 2*Q+1) throw std::logic_error("invalid pmf size");
		std::vector<double> tmpout;
		tmpout.resize(4*Q+1);
	
		for(int x=-2*Q; x<=2*Q; x++) {
			kahan_acc_t acc;
			for(int tau=-Q; tau<=Q; tau++) {
				if(x-tau>=-Q && x-tau<=Q) acc( X1.at(tau+Q)*X2.at(x-tau+Q) );
			}
			tmpout.at(x+2*Q)= sum_kahan(acc);
		}

		out.resize(2*Q+1);
		// saturate in [-Q,Q]
		kahan_acc_t acc;
		for(int x=-2*Q; x<=-Q; x++) acc( tmpout.at(x+2*Q) );
		out.at(0)= sum_kahan(acc);
		acc= kahan_acc_t(); // reset
		for(int x=Q; x<=2*Q; x++) acc( tmpout.at(x+2*Q) );
		out.at(2*Q)= sum_kahan(acc);
		// assign other values normally
		for(int x=-Q+1; x<=Q-1; x++) {
			out.at(x+Q)= tmpout.at(x+2*Q);
		}
	}

	double PMFerror(std::vector<double> const& pmf) {
		int Q = (pmf.size()-1)/2;
		if(2*Q+1 != pmf.size()) throw std::logic_error("PMF length must be odd");
		kahan_acc_t acc;
		for(int x=-Q; x<0; x++) acc(pmf.at(x+Q));
		acc(pmf.at(Q) * 0.5);
		return sum_kahan(acc);
	}

	void flipSign(std::vector<double>& pmfout,
	              std::vector<double> const& pmfin) {
		pmfout.resize(pmfin.size());
		for(int i=0; i<pmfin.size(); i++) {
			pmfout[pmfout.size()-i-1]= pmfin[i];
		}
	}

	void checkPMF(std::vector<double> const& pmf, int Q) {
		checkPMFSilent(pmf, Q);

		if(pmf.size() == 2*Q+1) {
			double perr= PMFerror(pmf);
			std::cerr<< "  The PMF error prob is "<<perr << std::endl;
		}
	}

	void checkPMFSilent(std::vector<double> const& pmf, int Q) {
		if(pmf.size() != Q+1 && pmf.size() != 2*Q+1) {
			std::cerr << "PMF has an invalid size!" <<std::endl;
		}
		kahan_acc_t acc;
		for(int j=0; j<pmf.size(); j++) acc( pmf[j] );
		double pmfSum= sum_kahan(acc);
		if(pmfSum < (1-1e-12) || pmfSum > (1+1e-12)) {
			std::cerr << "  PMF sum = "<<sum_kahan(acc)<<std::endl;
		}
		// make sure it contains no invalid values
		for(int j=0; j<pmf.size(); j++) {
			if(pmf[j]<0 || pmf[j]>1)
				std::cerr << "invalid pmf value ("<<pmf[j]<<") at i="<<j<<"!"<<std::endl;
		}
	}

	void checkCDF(std::vector<double> const& cdf) {
		// make sure cdf elements are in [0, 1]
		for(int i=0; i<cdf.size(); i++) {
			if(cdf[i]<0 || cdf[i]>1)
				std::cerr << "invalid cdf value ("<<cdf[i]<<") at i="<<i<<"!"<<std::endl;
		}
		// make sure cdf is increasing
		for(int i=1; i<cdf.size(); i++) {
			if(cdf[i]<cdf[i-1])
				std::cerr << "cdf is decreasing ("<<(cdf[i]-cdf[i-1])<<") at i="<<i<<"!"<<std::endl;
		}
		// make sure the last element is 1
		if(cdf[cdf.size()-1] < 1)
			std::cerr << "last element of cdf is "<<cdf[cdf.size()-1]<<"!"<<std::endl;
	}
	
} // end namespace DE
