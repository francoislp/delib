#include "ScheduleParser.hpp"
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <regex>

#ifndef NDEBUG
#include <iostream>
#endif

// Character indicating a line comment
#define COMMENTCHAR '#'

using std::string;
using std::ifstream;
using std::stringstream;
using std::regex;
using std::regex_search;

void ScheduleParser::parseFile(std::string filepath) {
	ifstream ifs(filepath.c_str());
  if(!ifs.good()) throw file_exception(filepath);

  // regular expression for matching lines
  regex r("t=([[:digit:]]+):[[:space:]]*(.+)[[:space:]]*,");

  string curLine;
  while(ifs.good()) {
    getline_nc(ifs, curLine);
    std::smatch tokens; // returns matches as std::string objects
    if(regex_search(curLine, tokens, r)) {
	    stringstream ss(tokens[1]);
	    int iter=-1;
	    ss >> iter;
	    string confName= tokens[2];

	    // make sure iterations appear in order (note: in the file, the
	    // first rule is associated with iter t=1)
	    if(iter!=m_confList.size()+1) throw syntax_exception(curLine);

	    m_confList.push_back(confName);
    }
  }

#ifndef NDEBUG
  std::cerr<< "ScheduleParser: Found "<<m_confList.size()<<" valid lines.";
  std::cerr<< std::endl;
#endif
}

int ScheduleParser::getNbIter() {
	return m_confList.size();
}

std::string ScheduleParser::getConfForIter(int iter) {
	if(iter<0) throw std::runtime_error("Iteration index cannot be negative");
	return m_confList.at(iter);
}

// ---------- (private) ----------

// (line parsing code taken from config.cpp)

void ScheduleParser::getline_nc(ifstream& ifs, string& line) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!ifs.eof()) std::getline(ifs, line);
    else line = "";
    stringstream sstream;
    sstream << line;
    sstream >> std::ws; // extract whitespace
    firstchar = sstream.peek();
  }
  // trim leading and trailing whitespace
  line = trim(line);
}

// http://stackoverflow.com/questions/25829143/c-trim-whitespace-from-a-string
std::string& ScheduleParser::trim(std::string & str)
{
   return ltrim(rtrim(str));
}

std::string& ScheduleParser::ltrim(std::string & str)
{
  auto it2 =  std::find_if( str.begin() , str.end() , [](char ch){ return !std::isspace<char>(ch , std::locale::classic() ) ; } );
  str.erase( str.begin() , it2);
  return str;   
}

std::string& ScheduleParser::rtrim(std::string & str)
{
  auto it1 =  std::find_if( str.rbegin() , str.rend() , [](char ch){ return !std::isspace<char>(ch , std::locale::classic() ) ; } );
  str.erase( str.begin() + (  str.rend() - it1 ) , str.end() );
  return str;   
}
