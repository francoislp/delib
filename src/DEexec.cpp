/**
 * Author: Francois Leduc-Primeau
 */

#include "config/config.hpp"
#include "DE/DE.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include "DE/ScheduleParser.hpp"

const std::string btwcDir = ""; // TODO: should be defined in the config

using std::cout;
using std::cerr;
using std::endl;
using std::string;

// Forward declarations
void setAuthorizedKeys(config& conf);

int main(int argc, char** argv) {

	if(argc<=1) {
		cerr << "Usage: DE <mandatory items> [<optional items>]" << endl;
		cerr << "Mandatory config items:" << endl;
		cerr << "  {exitgen=<path to ExitGen config>,"<<endl;
		cerr << "   DE::nbiter=<number of DE iterations>}"<<endl;
		cerr << "           OR"<<endl;
		cerr << "   DE::sched=<filepath>"<<endl;		
		cerr << "Optional config items:" << endl;
		cerr << "  - LLRDeviation::dev_pmf_file=<path to cond PMF file>"<<endl;
		cerr << "  - LLRDeviation::dev_vntotal_pmf_file=<filepath>"<<endl;
		cerr << "  - DE::Q=<max msg magnitude>"<<endl;
		cerr << "  - DE::p_in=<channel error prob>"<<endl;
		cerr << "  - DE::msgPerr=<initial message error prob>"<<endl;
		cerr << "  - DE::ET_N=<code length>"<<endl;
		cerr << "Optional switches:" <<endl;
		cerr << "  --vnTotal" <<endl;
		return 1;
	}

	config conf;
	setAuthorizedKeys(conf);

  // parse the command line arguments
  try {
    conf.initCL(argc, argv);
  } catch(syntax_exception& e) {
    cerr << "Invalid syntax in following: "<<e.what()<<endl;
    return 1;
  } catch(invalidkey_exception& e) {
    cerr << "Invalid key: "<<e.what()<<endl;
    return 1;
  }

  // Load the content of the ExitGen configuration
  bool useSchedule= false;
  try {
    string exitgenPath = conf.getParamString("exitgen");
    conf.initFile(exitgenPath);
  } catch(key_not_found& e) {
	  useSchedule= true;
  }

  int nbDEiter; // number of DE iterations
  bool reportVnTotal= false;
  ScheduleParser schedParser;

  DE::DE de;

  if(useSchedule) {
	  if(conf.checkOption("vnTotal")) {
		  cerr << "--vnTotal not currently supported for schedules."<<endl;
		  return 1;
	  }
	  string schedPath;
	  try {
		  schedPath= conf.getParamString("DE::sched");
	  } catch(key_not_found& e) {
		  cerr << "Missing mandatory key: "<<e.what() << endl;
		  return 1;
	  }

	  try {
		  schedParser.parseFile(schedPath);
	  } catch(file_exception& e) {
		  cerr<< "Problem reading "<< e.what() <<endl;
		  return 1;
	  } catch(syntax_exception& e) {
		  cerr<< "Syntax error in schedule file: "<< e.what() << endl;
		  return 1;
	  }

	  cerr << "*Using a rule schedule*" << endl;
	  cout << "# Using schedule "<<schedPath << endl;

	  nbDEiter= schedParser.getNbIter();
	  if(nbDEiter<1) {
		  cerr << "Invalid schedule" <<endl;
		  return 1;
	  }
	  // load parameters from first exitgen config file
	  std::stringstream ss;
	  ss<< btwcDir << "/exit_conf/" << schedParser.getConfForIter(0);
#ifndef NDEBUG
	  cerr<< "First EXITgen config: "<<ss.str() <<endl;
#endif
	  conf.initFile(ss.str());
	  try {
		  de.loadParameters(conf);
	  } catch(key_not_found& e) {
		  cerr << "Mandatory key not found: "<<e.what() <<endl;
		  return 1;
	  }
  }
  else { // (not using a schedule)
	  try {
		  de.loadParameters(conf);
		  nbDEiter = conf.parseParamUInt("DE::nbiter");
	  } catch(key_not_found& e) {
		  cerr << "Mandatory key not found: "<<e.what() <<endl;
		  return 1;
	  }

	  // check for optional parameters
	  if(!conf.keyExists("LLRDeviation::dev_pmf_file")) {
		  cerr << "***Deviations will NOT be applied" << endl;
		  cout << "# (no deviations applied)" << endl;
	  } else {
		  string devPMFFile= conf.getParamString("LLRDeviation::dev_pmf_file");
		  cout << "# Deviation file: "<< devPMFFile << endl;
		  de.applyDev(devPMFFile);
	  }

	  if(conf.checkOption("vnTotal")) {
		  reportVnTotal= true;
		  de.computeTotalPMF(true);
		  cout << "# Reporting VN-total PMF" <<endl;
		  if(!conf.keyExists("LLRDeviation::dev_vntotal_pmf_file")) {
			  cerr << "***Deviations will NOT be applied on VN total"<<endl;
			  cout << "# (no deviations applied on VN total)"<<endl;
		  } else {
			  string devPMFFile= conf.getParamString("LLRDeviation::dev_vntotal_pmf_file");
			  cout << "# VN-total deviation file: "<< devPMFFile <<endl;
			  de.applyDevTotal(devPMFFile);
		  }
	  }
  } // end: not using a schedule

  // this mode checks if a 1-D approximation of the input distribution
  // yields a pessimistic error probability.
  bool doCheck1D= conf.checkOption("check1D");
  //if(doCheck1D) cerr << "Checking if 1D approx. is pessimistic"<<endl;
  if(doCheck1D) cerr << "--check1D is not implemented!" <<endl;

  // heuristic to predict the BER when early termination is used in
  // the decoder (requires --vnTotal and applies only to DE with
  // deviations)
  bool earlyTermMode= false;
  int earlyTermN= 0; // code length used to estimate FER
  double prevDETotalBER= -1; // previous BER reported by DE
  double earlyTermBER, earlyTermFER;
  if(conf.keyExists("DE::ET_N")) {
	  if(!conf.keyExists("LLRDeviation::dev_pmf_file") ||
	     !conf.keyExists("LLRDeviation::dev_vntotal_pmf_file") ||
	     !conf.checkOption("vnTotal")) {
		  cerr << "Early termination heuristic requires deviations and VN-total output" << endl;
		  return 1;
	  }
	  earlyTermN= conf.parseParamUInt("DE::ET_N");
	  if(earlyTermN<=0) {
		  cerr << "Code length for early termination heuristic must be >=1"<<endl;
		  return 1;
	  }
	  earlyTermMode= true;
	  cerr << "***Using early termination heuristic" <<endl;
  }
  
  // range of quantized messages is [-Q, Q]
  try {
	  int Q= conf.parseParamUInt("DE::Q");
	  cerr << "***Overriding message range!" << endl;
	  de.setQ(Q);
  } catch(key_not_found&) {}
  int Q= de.getQ();
  int Qtotal= de.getQtotal();
  cerr << "Message range is ["<<-Q<<", "<<Q<<"]" <<endl;
  cout << "# Message range is ["<<-Q<<", "<<Q<<"]" <<endl;
  cerr << "VN-total range is ["<<-Qtotal<<", "<<Qtotal<<"]"<<endl;
  cout << "# VN-total range is ["<<-Qtotal<<", "<<Qtotal<<"]"<<endl;  

  try {
	  double chDistr= conf.parseParamDouble("DE::p_in");
	  cerr << "***Overriding channel error prob!"<<endl;
	  de.setChDistr(chDistr);
  } catch(key_not_found&) {}

  try {
	  double msgErr= conf.parseParamDouble("DE::msgPerr");
	  cerr<< "***Starting with msg error rate "<<msgErr<<endl;
	  de.setInitialMsgDistr(msgErr);
  } catch(key_not_found&) { }  

  de.DEInit();
  
  cerr << "Running "<<nbDEiter<<" DE iterations"<<endl;
  cout << "##############################"<<endl;
  if(earlyTermMode) cout << "# Iter completed - BER - FER #"<<endl;
  else              cout << "# Iter completed - p_e - PMF #"<<endl;
  cout << "##############################"<<endl;
  
  // print statistics before first iteration
  // Note: At this point, the VN total, the channel prior, and the
  // outgoing VN messages are all equal, but they don't have the same
  // domain so we still need to call the appropriate function.
  if(earlyTermMode) {
	  cout << "0  " << de.curTotalErrProb() << "  ";
	  cout << 1-pow(1-de.curTotalErrProb(), earlyTermN) << endl;
  } else if(reportVnTotal) {
	  cout << "0  " << de.curTotalErrProb();
	  de.printTotalPMF(cout);
  } else {
	  cout << "0  " << de.currentErrProb();
	  de.printMsgPMF(cout);
  }
  
  // DE iterations
  for(int i=0; i<nbDEiter; i++) {

	  if(useSchedule) { // load deviation distribution for current iteration
		  // load current ExitGen config
		  std::stringstream ss;
		  ss<< btwcDir << "/exit_conf/" << schedParser.getConfForIter(i);
#ifndef NDEBUG
		  cerr<< "ExitGen config at iter "<<i<<": "<<ss.str()<<endl;
#endif
		  config curConf;
		  setAuthorizedKeys(curConf);
		  curConf.initFile(ss.str());

		  // retrieve path of deviation file
		  ss= std::stringstream();
		  // ss << btwcDir <<"/"<< curConf.getParamString("devdistr_filepath_default");
		  // curConf.addConfElem("LLRDeviation::dev_file", ss.str());
		  ss << btwcDir << "/" << curConf.getParamString("devpmf_filepath_default");

		  de.applyDev(ss.str());
	  } //end: load deviation distribution
	  
	  de.iteration();

	  if(earlyTermMode) {
		  double curDETotalBER= de.curTotalErrProb();
		  double curDEFER= 1-pow(1-curDETotalBER, earlyTermN);
		  if(i>0 &&
		     fabs(prevDETotalBER/curDETotalBER-1) < 1) { // BER is deemed stable
			  earlyTermFER= curDEFER * earlyTermFER;
			  if(earlyTermFER<1) {
				  earlyTermBER= 1 - exp(log(1-earlyTermFER)/earlyTermN);
			  } else {
				  earlyTermBER= de.curTotalErrProb();
			  }
		  } else {
			  earlyTermFER= curDEFER;
			  earlyTermBER= de.curTotalErrProb();
		  }
		  prevDETotalBER= curDETotalBER;
	  }
	  
	  // print result
	  if(earlyTermMode) {
		  // (output format is different)
		  cout << de.iterationCount() << "  " << earlyTermBER;
		  cout << "  " << earlyTermFER << endl;
	  } else if(reportVnTotal) {
		  cout << de.iterationCount() << "  " << de.curTotalErrProb();
		  de.printTotalPMF(cout);
	  } else {
		  cout << de.iterationCount() << "  " << de.currentErrProb();
		  de.printMsgPMF(cout);
	  }

	  //TODO: this feature could be done using a second DE object...
	  // if(doCheck1D) { // compare error prob. with 1-D input distribution
		//   double curRho= boost::math::erf_inv(1.0-2.0*curPErr);
		//   curRho= 4.0 * curRho * curRho * _fractMult;
		//   vector<double> testPMF;
		//   testPMF.resize(2*Q+1);
		//   normPMF1Dsat(Q, curRho, alpha, testPMF);
		//   // CN step
		//   cnPMF(testPMF, testPMF, _d_c, _omsOffset, Q);
		//   // VN step
		//   vnPMF(testPMF, testPMF, Q, chPMF, Qtotal, _d_v);
		//   // deviation step
		//   if(applyDev) {
		// 	  pLLRDev->setInputErrorRate(curPErr); // (to be safe)
		// 	  devPMF(testPMF, testPMF, Q, pLLRDev, _fractMult);
		//   }
		//   double testErrProb= PMFerror(testPMF);
		//   if(testErrProb < perr) {
		// 	  cerr<< "1-D p_err is lower at iter="<<i+1;			  
		// 	  cerr<< " (rel. err. "<< (testErrProb-perr)/perr <<")"<<endl;
		//   }
	  // }
  } //end: DE iterations
} //end: main



/**
 * Sets up a list of keys that are allowed to be found in a configuration.
 */
void setAuthorizedKeys(config& conf) {
	// list of valid configuration keys (TODO: instead, the config class
	// should have the ability to load a list of valid keys from a
	// file.)
  conf.addValidKey("exitgen");
  conf.addValidKey("DE::nbiter");
  conf.addValidKey("DE::Q"); // to override the message range
  conf.addValidKey("DE::p_in"); // to override the channel error prob
  // to specify an initial 1-D normal distribution for messages that
  // is different from the channel distribution:
  conf.addValidKey("DE::msgPerr");
  conf.addValidKey("DE::sched");
  conf.addValidKey("DE::msg_sched"); // message-passing schedule
  conf.addValidKey("DE::ET_N"); // BER with early termination heuristic
  conf.addValidOption("check1D");
  conf.addValidOption("vnTotal");
  // ExitGen config
  conf.addValidKey("QLLRmagn");
  conf.addValidKey("QLLRmsg");
  conf.addValidKey("QLLRtotal");
  conf.addValidKey("QLLRfract");
  conf.addValidKey("DC");
  conf.addValidKey("OMSOFFSET");
  conf.addValidKey("CUT_LATENCY");
  conf.addValidKey("DV");
  conf.addValidKey("nb_iter");
  conf.addValidKey("ch_dist");
  conf.addValidKey("x_param_list");
  conf.addValidKey("iv_filepath");
  conf.addValidKey("ov_filepath");
  conf.addValidKey("use_rnd_tx_cw");
  conf.addValidKey("fixed_cw");
  conf.addValidKey("use_nonzero_Li");
  conf.addValidKey("opcon_name");
  conf.addValidKey("opcon_name_nominal");
  conf.addValidKey("Tclk");
  conf.addValidKey("Tclk_nominal");
  conf.addValidKey("design_name");
  conf.addValidKey("exit_filepath");
  conf.addValidKey("devdistr_filepath_default");
  conf.addValidKey("devpmf_filepath_default");
  conf.addValidKey("MC_DE_nbiter");
  conf.addValidKey("MC_DE_trialsPerIter");
  conf.addValidKey("iv_DE_filepath");
  conf.addValidKey("ov_DE_filepath");
  conf.addValidKey("top_cell");
  conf.addValidKey("deviations_on_total");
  
  conf.addValidKey("LLRDeviation::dev_pmf_file");
  conf.addValidKey("LLRDeviation::dev_vntotal_pmf_file");
  conf.addValidKey("LLRDeviation::nbErrPtsPerDec");
  conf.addValidKey("LLRDeviation::fract_mult");
}

	
