#include "DevPMFFileParser.hpp"
#include "CustomExceptions.hpp"
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <set>

// Character indicating a line comment
#define COMMENTCHAR '#'

using std::string;
using std::ifstream;
using std::stringstream;
using std::vector;

DevPMFFileParser::DevPMFFileParser() {
	m_Q= 0;
}

void DevPMFFileParser::parseFile(std::string filepath) {
	ifstream ifs(filepath.c_str());
  if(!ifs.good()) throw file_exception(filepath);

  std::set<double> pInSet;

  string curLine;
  while(ifs.good()) {
    getline_nc(ifs, curLine);
    if(curLine != "") {
      stringstream ss;
      ss << curLine;

      double curPIn;
      // extract p_e
      ss >> curPIn;
      if(ss.fail()) throw syntax_exception(curLine);
      // perform some checks on the value
      if(curPIn<=0 || curPIn >=1) throw std::runtime_error("p_e check fails");
      pInSet.insert(curPIn);

      // ideal output
      int curNu;
      ss >> curNu;
      if(ss.fail()) throw syntax_exception(curLine);
      // assume that the first ideal message value encountered is the
      // smallest (negative) message value
      if(m_Q==0) m_Q= -curNu;
      if(curNu<-m_Q || curNu>m_Q) throw syntax_exception(curLine);
      //TODO: Should check that nu (ideal) values are seen in order

      // read PMF containing 2*m_Q+1 elements
      // first make sure the vectors are sized properly
      if(m_pmfs[curPIn].size() < 2*m_Q+1) m_pmfs[curPIn].resize(2*m_Q+1);
      vector<double>& curPmf= m_pmfs[curPIn].at(curNu+m_Q);
      curPmf.resize(2*m_Q+1);
      // then copy the data
      for(int i=0; i<curPmf.size(); i++) {
	      ss >> curPmf[i];
	      if(ss.fail()) throw syntax_exception(curLine);
      }
    }
  }

  // copy p_in set to vector
  m_pInList.resize(pInSet.size());
  std::copy(pInSet.begin(), pInSet.end(), m_pInList.begin());
}

// (private)
void DevPMFFileParser::getline_nc(ifstream& ifs, string& line) {
  char firstchar = COMMENTCHAR;

  while(firstchar == COMMENTCHAR) {
    if(!ifs.eof()) std::getline(ifs, line);
    else line = "";
    stringstream sstream;
    sstream << line;
    sstream >> std::ws; // extract leading whitespace
    firstchar = sstream.peek();
  }
}
